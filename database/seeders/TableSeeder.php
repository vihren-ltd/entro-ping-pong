<?php

namespace Database\Seeders;

use App\Models\Table;
use Illuminate\Database\Seeder;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Table::create([
            'name' => 'Table 1',
            'active' => true
        ]);

        Table::create([
            'name' => 'Table 2',
            'active' => true
        ]);

        Table::create([
            'name' => 'Table 3',
            'active' => true
        ]);

        Table::create([
            'name' => 'Table 4',
            'active' => true
        ]);
    }
}
