<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;

    protected $fillable = [
        'table_id',
        'date',
        'time',
        'name',
        'email',
        'phone',
        'reservation_code'
    ];

    public function table()
    {
        return $this->belongsTo(Table::class);
    }
}
