<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\Table;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use stdClass;
use Symfony\Component\HttpFoundation\Response;

class ReservationController extends Controller
{

    public function showDate(string $date = '', string $time = '')
    {
        if (empty($date)) {

            $currentDate = CarbonImmutable::today();

        } else {

            try
            {

                $currentDate = new CarbonImmutable($date);

                if (! empty($time))
                {
                    $time = new CarbonImmutable($time);
                }

            } catch (\Carbon\Exceptions\InvalidFormatException $e) {

                abort(Response::HTTP_BAD_REQUEST);

            }
        }

        $todaySlots = $this->generateEmptyTimeSlotsArray(
            config('reservations.table_first_slot'),
            config('reservations.table_last_slot')
        );

        $reservations = Reservation::where('date', $currentDate->toDateString())->get();

        $tables = Table::where('active', true)->get();

        foreach($reservations as $reservation)
        {

            if (!empty($todaySlots[$reservation->time]))
            {
                $slotToReserve = $todaySlots[$reservation->time];

                if (array_search($reservation->table_id, $slotToReserve->tables) === false) {

                    // --- Check if the reservation is for an active table --- //
                    if($tables->find($reservation->table_id))
                    {
                        $slotToReserve->tables[] = $reservation->table_id;

                        if(sizeof($slotToReserve->tables) >= $tables->count())
                        {
                            $slotToReserve->full = true;
                        }
                    }


                } else {
                    /* --- Duplicate reservation!
                        This does not concern the front-end user as the table is busy anyway.
                        To be considered some alerts for duplicate reservations in the admin panel --- */
                }

            } else {
                /* --- Reservation is out of the configured time slots!
                    Good to consider how to handle this. Currently, just skipping it. --- */
            }
        }

        // --- Check if time selected is a valid time slot --- //
        if (!empty($time)) {

            $time = $time->format('H:i');

            if(! array_key_exists($time, $todaySlots))
            {
                abort(Response::HTTP_BAD_REQUEST);
            }
        }

        return view('reservation-show-date', [
            'current_date' => $currentDate,
            'current_time' => $time,
            'today_slots' => $todaySlots,
            'tables' => $tables
        ]);
    }

    public function showForm(string $date, string $time, Table $table)
    {
        [$date, $time] = $this->validateDateTime($date, $time);

        return view('reservation-show-form', [
            'current_date' => $date,
            'current_time' => $time,
            'table' => $table
        ]);

    }

    public function saveReservation(Request $request, string $date, string $time, Table $table)
    {

        [$date, $time] = $this->validateDateTime($date, $time);

        $validated = Validator::make($request->input(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'nullable',
        ])->validate();

        // --- Check if reservation already exists --- //
        if (Reservation::where('table_id', $table->id)
            ->where('date', $date->toDateString())
            ->where('time', $time->format('H:i'))
            ->first()) {

            return redirect(route('date.show', [$date->toDateString(), $time->format('H:i')]))
                ->with('error', __('Attempt to make duplicate reservation!'));
        }

        $reservation = new Reservation([
            'table_id' => $table->id,
            'date' => $date->toDateString(),
            'time' => $time->format('H:i'),
            'name' => $validated['name'],
            'email' => $validated['email'],
            'phone' => $validated['phone'],
            'reservation_code' => ''
        ]);

        if (! $reservation->save()) {

            return redirect(route('date.show', [$date->toDateString(), $time->format('H:i')]))
                ->with('error', __('There was problem saving your reservation!'));

        }

        // --- Generate and save the reservation code --- //
        $reservation->reservation_code = 'PP-1' . str_pad($reservation->id, 4, '0', STR_PAD_LEFT);
        $reservation->save();

        return redirect(route('reservation.confirm', $reservation->id));

    }

    public function confirmReservation(Reservation $reservation)
    {
        return view('reservation-show-confirmation', [
            'reservation' => $reservation
        ]);
    }

    protected function generateEmptyTimeSlotsArray( string $firstSlot, string $lastSlot)
    {
        $firstSlot = new Carbon($firstSlot);
        $lastSlot = new Carbon($lastSlot);

        $currentSlot = $firstSlot;
        $slotsArray = [];

        while($currentSlot <= $lastSlot) {

            $slot = new stdClass();
            $slot->time = $currentSlot->format('H:i');
            $slot->time_frame = $currentSlot->format('H:i');
            $slot->tables = [];
            $slot->full = false;

            $slotsArray[$currentSlot->format('H:i')] = $slot;

            $currentSlot->addHour();

            $slot->time_frame .= ' - ' . $currentSlot->format('H:i');

        }

        return $slotsArray;
    }

    protected function validateDateTime(string $date, string $time)
    {
        try
        {
            $date = new CarbonImmutable($date);
            $time = new CarbonImmutable($time);

        } catch (\Carbon\Exceptions\InvalidFormatException $e) {

            abort(Response::HTTP_BAD_REQUEST);

        }

        $todaySlots = $this->generateEmptyTimeSlotsArray(
            config('reservations.table_first_slot'),
            config('reservations.table_last_slot')
        );

        if (empty($todaySlots[$time->format('H:i')]))
        {
            abort(Response::HTTP_BAD_REQUEST);
        }

        return [$date, $time];
    }
}
