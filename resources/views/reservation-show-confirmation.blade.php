<x-app>

    <div class="container">

        <h1 class="text-center mt-4">Ping-Pong Table Resevation</h1>

        <hr>

        <h2 class="my-5 text-success text-center">Your reservation is successfull</h2>

        <h3 class="text-center mb-4">Reservation Code: <strong class="text-primary">{{ $reservation->reservation_code }}</strong></h3>

        <p class="text-center mb-4">Please save this code and present it to our staff when you come the venue.</p>

        <hr>

        <p class="text-center my-4">
            <a
                class="btn btn-outline-primary btn-lg"
                href="{{ route('date.show', $reservation->date) }}"
            >
                Book more tables <i class="la la-arrow-right"></i>
            </a>
        </p>

    </div>

</x-app>
