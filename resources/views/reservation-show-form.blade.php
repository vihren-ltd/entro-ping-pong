<x-app>

    <div class="container">

        <h1 class="text-center mt-4">Ping-Pong Table Resevation</h1>

        <x-nav-dates :currentDate="$current_date" />

        <h3 class="mb-5">Please complete your reservation details:</h3>

        <form method="post" action="{{ route('reservation.submit', [$current_date->toDateString(), $current_time->format('H:i'), $table->id]) }}">
            @csrf

            <div class="form-group row mb-4">
                <label for="name" class="col-form-label col-sm-4 text-right">Reservation for:</label>
                <div class="col-sm-5">
                    <h3><strong class="text-primary">{{ $current_date->format('d.m.Y') . ', ' . $current_time->format('H:i') }}</strong></h3>
                </div>
            </div>

            <div class="form-group row mb-4">
                <label for="name" class="col-form-label col-sm-4 text-right">Table:</label>
                <div class="col-sm-5">
                    <h3><strong>{{ $table->name }}</strong></h3>
                </div>
            </div>

            <div class="form-group row mb-4">
                <label for="name" class="col-form-label col-sm-4 text-right">Your name:</label>
                <div class="col-sm-5">
                    <input
                        type="text"
                        class="form-control form-control-lg"
                        name="name"
                        id="name"
                        value="{{ request()->old('name') }}"
                        autofocus
                        required
                    >
                </div>
            </div>

            <div class="form-group row mb-4">
                <label for="email" class="col-form-label col-sm-4 text-right">E-mail:</label>
                <div class="col-sm-5">
                    <input
                        type="email"
                        class="form-control form-control-lg"
                        name="email"
                        id="email"
                        value="{{ request()->old('email') }}"
                        required
                    >
                </div>
            </div>

            <div class="form-group row mb-4">
                <label for="phone" class="col-form-label col-sm-4 text-right">Phone:</label>
                <div class="col-sm-5">
                    <input
                        type="text"
                        class="form-control form-control-lg"
                        name="phone"
                        id="phone"
                        value="{{ request()->old('phone') }}"
                    >
                </div>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <hr>

            <button
                class="btn btn-primary btn-lg float-right"
                type="submit"
            >
                Submit Your Reservation <i class="la la-arrow-right"></i>
            </button>

        </form>

    </div>

</x-app>
