<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('table') }}'><i class='nav-icon la la-table-tennis'></i> Tables</a></li>


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('reservation') }}'><i class='nav-icon las la-calendar-check'></i> Reservations</a></li>
