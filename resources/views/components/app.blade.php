<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Entro Ping Pong</title>

    <!-- Styles -->
    <link rel="stylesheet" id="css-main" href="{{ mix('css/app.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('packages/line-awesome/css/line-awesome.min.css') }}">
</head>
<body class="p-0">

<header class="bg-dark text-white p-3">

    <div class="row justify-content-between align-items-center">

        <div class="col-auto">
            <h3 class="m-0">Entro Solutions</h3>
        </div>

        <div class="col-auto text-right">
            <a
                class="btn btn-outline-secondary text-white"
                href="{{ route('backpack') }}"
            >
                Admin
            </a>
        </div>

    </div>

</header>

{{ $slot }}

<script src="{{ mix('/js/app.js') }}"></script>

</body>
</html>
