<hr>

<div class="row justify-content-between align-items-center">

    <div class="col text-left">
        <a
            class="btn btn-outline-secondary btn-lg{{ $currentDate->subDay(2) < \Illuminate\Support\Carbon::today() ? " disabled" : "" }}"
            href="{{ route('date.show', $currentDate->subDay(2)->toDateString()) }}"
        >
            <i class="las la-angle-double-left"></i>
            {{ $currentDate->subDay(2)->toFormattedDateString() }}
        </a>
    </div>

    <div class="col text-left">
        <a
            class="btn btn-outline-secondary btn-lg{{ $currentDate->subDay() < \Illuminate\Support\Carbon::today() ? " disabled" : "" }}"
            href="{{ route('date.show', $currentDate->subDay()->toDateString()) }}"
        >
            <i class="las la-angle-left"></i>
            {{ $currentDate->subDay()->toFormattedDateString() }}
        </a>
    </div>

    <div class="col text-center h3 m-0 text-primary">
        <strong>{{ $currentDate->toFormattedDateString() }}</strong>
    </div>

    <div class="col text-right">
        <a
            class="btn btn-outline-secondary btn-lg"
            href="{{ route('date.show', $currentDate->addDay()->toDateString()) }}"
        >
            {{ $currentDate->addDay()->toFormattedDateString() }}
            <i class="las la-angle-right"></i>
        </a>
    </div>

    <div class="col text-right">
        <a
            class="btn btn-outline-secondary btn-lg"
            href="{{ route('date.show', $currentDate->addDay(2)->toDateString()) }}"
        >
            {{ $currentDate->addDay(2)->toFormattedDateString() }}
            <i class="las la-angle-double-right"></i>
        </a>
    </div>

</div>

<hr>
