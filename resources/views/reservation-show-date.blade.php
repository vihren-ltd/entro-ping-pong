<x-app>

    <div class="container">

        <h1 class="text-center mt-4">Ping-Pong Table Resevation</h1>

        <x-nav-dates :currentDate="$current_date" />

        <div class="row">

            <div class="col-sm-4">

                <div id="hour-slots" class="bg-dark text-white h-100 p-3">
                    <h4>{{ __('Please Choose Desired Time:') }}</h4>

                    <div id="time-slots-container" class="mt-3">
                        @foreach($today_slots as $time_slot)
                            <a
                                class="btn btn-light btn-block
                                    {{
                                        $time_slot->full
                                            ? " text-danger disabled"
                                            : ( $time_slot->time == $current_time ? "bg-info" : "")
                                    }}
                                "
                                href="{{ route('date.show', [$current_date->toDateString(), $time_slot->time]) }}">
                                {{ $time_slot->time_frame }}
                            </a>
                        @endforeach


                    </div>

                </div>

            </div>

            <div class="col-sm-8">

                @if(!empty($current_time))
                    <h3>Selected time: <span class="text-primary">{{ $current_time }}</span></h3>

                    <hr>

                    @if(session()->get('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif

                    <div class="row">
                        @forelse($tables as $table)

                            @php
                                $tableBusy = ( array_search($table->id, $today_slots[$current_time]->tables) !== false);
                            @endphp

                            <div class="col-sm-6 col-md-4 mb-4">
                                <div class="table-box border rounded text-center py-5 px-2 h-100">

                                    <h5 class="mb-3">{{ $table->name }}</h5>

                                    @if(! $tableBusy)
                                        <a
                                            class="btn btn-outline-primary"
                                            href="{{ route('reservation.form', [$current_date->toDateString(), $current_time, $table->id]) }}"
                                        >
                                            Select this table <i class="la la-arrow-right"></i>
                                        </a>
                                    @else
                                        <p class="text-danger">This table is already booked</p>
                                    @endif

                                </div>
                            </div>

                        @endforeach
                    </div>


                @endif

            </div>

        </div>

    </div>

</x-app>
