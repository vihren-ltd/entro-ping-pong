<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/reservations/{reservation}', ['App\Http\Controllers\ReservationController', 'confirmReservation'])->name('reservation.confirm');

Route::get('/{date?}/{time?}', ['App\Http\Controllers\ReservationController', 'showDate'])->name('date.show');

Route::get('/{date}/{time}/{table}', ['App\Http\Controllers\ReservationController', 'showForm'])->name('reservation.form');
Route::post('/{date}/{time}/{table}', ['App\Http\Controllers\ReservationController', 'saveReservation'])->name('reservation.submit');

