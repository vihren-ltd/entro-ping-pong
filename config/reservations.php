<?php

return [

    'table_first_slot' => env('RESERVATIONS_TABLE_SLOT_FIRST', '09:00'),

    'table_last_slot' => env('RESERVATIONS_TABLE_SLOT_LAST', '21:00'),

];
